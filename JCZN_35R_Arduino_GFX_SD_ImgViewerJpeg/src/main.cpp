#include <Arduino.h>
#include <Arduino_GFX_Library.h>

const char *jpeg[6] = {"/0.jpg", "/1.jpg", "/2.jpg", "/3.jpg", "/4.jpg","/5.jpg"};

#define GFX_BL 27 // default backlight pin, you may replace DF_GFX_BL to actual backlight pin

Arduino_DataBus *bus = new Arduino_ESP32SPI(2 /* DC */, 15 /* CS */, 14 /* SCK */, 13 /* MOSI */, 12 /* MISO */, HSPI /* spi_num */);
Arduino_GFX *gfx = new Arduino_ST7796(bus, -1 /* RST */, 3 /* rotation */, false /* IPS */,
                                      320 /* width */, 480 /* height */,
                                      0 /* col offset 1 */, 0 /* row offset 1 */);

#include <SD.h>

#define SD_MOSI 23
#define SD_MISO 19
#define SD_SCK 18
#define SD_CS 5

SPIClass SDSPI(VSPI);

#include "JpegFunc.h"

// pixel drawing callback
static int jpegDrawCallback(JPEGDRAW *pDraw)
{
  // Serial.printf("Draw pos = %d,%d. size = %d x %d\n", pDraw->x, pDraw->y, pDraw->iWidth, pDraw->iHeight);
  gfx->draw16bitBeRGBBitmap(pDraw->x, pDraw->y, pDraw->pPixels, pDraw->iWidth, pDraw->iHeight);
  return 1;
}

void setup()
{
  Serial.begin(115200);
  // Serial.setDebugOutput(true);
  // while(!Serial);
  Serial.println("Arduino_GFX JPEG Image Viewer example");

  // Init Display
  if (!gfx->begin())
  {
    Serial.println("gfx->begin() failed!");
  }
  gfx->fillScreen(BLACK);

  pinMode(GFX_BL, OUTPUT);
  digitalWrite(GFX_BL, HIGH);

  pinMode(5 /* CS */, OUTPUT);
  digitalWrite(5 /* CS */, HIGH);
  SDSPI.begin(SD_SCK, SD_MISO, SD_MOSI, SD_CS);
  // Initialise SD before TFT
  if (!SD.begin(SD_CS, SDSPI, 40000000))
  {
    Serial.println(F("ERROR: File System Mount Failed!"));
    gfx->println(F("ERROR: File System Mount Failed!"));
  }

  delay(5000);
}

void loop()
{
  int w = gfx->width();
  int h = gfx->height();
  
  for (int i = 0; i < 6; i++)
  {
    jpegDraw(jpeg[i], jpegDrawCallback, true /* useBigEndian */, 0, 0, w, h);       
    delay(5000);
    
    gfx->fillScreen(BLUE);
  }
}